const { Sequelize } = require("sequelize");
const {database}=require('./config_db');

//configuración instancia de Sequelize 
const sequelize = new Sequelize(database.database,database.username,database.password, {
  host: database.host,
  dialect: database.dialect,
  //query:{raw:true}
});

//Verificación conexión a la Base de datos
async function validarConexion (){
    try{
    await sequelize.authenticate();
    console.log("Conexion satisfactoria");
    }catch(error){
        console.error("Conexion con problemas",error); 
    }

}
validarConexion();

module.exports={sequelize}