require('dotenv').config();
module.exports={
  database:{
    username: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DATABASE,
    host: process.env.MYSQL_HOST,
    dialect: process.env.MYSQL_DIALECT,
  },
  server_port:{
    port:process.env.SERVER_PORT || 3000
  }
}
 