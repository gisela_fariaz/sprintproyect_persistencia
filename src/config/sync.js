'use strict'
const {Sequelize}=require('sequelize');
const{sequelize}=require('./conexion');

const usuarioModelo= require('../modelos/usuarios');
const productosModelo=require('../modelos/productos');
const estadosModelo=require('../modelos/estados');
const formas_pagosModelo=require('../modelos/formas_pagos');
const detalle_pedidosModelo=require('../modelos/detalle_pedidos');
const pedidosModelo=require('../modelos/pedidos');


const Usuarios=usuarioModelo(sequelize,Sequelize);
const Productos=productosModelo(sequelize,Sequelize);
const Estados=estadosModelo(sequelize,Sequelize);
const Formas_Pagos=formas_pagosModelo(sequelize,Sequelize);
const Detalle_Pedidos=detalle_pedidosModelo(sequelize,Sequelize);
const Pedidos=pedidosModelo(sequelize,Sequelize);

//Configuración de relaciones
Pedidos.belongsTo(Usuarios,{foreignKey:'id_usuario'});
Pedidos.belongsTo(Estados,{foreignKey:'id_estado'});
Pedidos.belongsTo(Detalle_Pedidos,{foreignKey:'id_detalle'});
Detalle_Pedidos.belongsTo(Formas_Pagos,{foreignKey:'id_pago'});
Detalle_Pedidos.belongsTo(Productos,{foreignKey:'id_producto'});

sequelize.sync({force:false}).then(()=>{
    console.log('Tablas sincronizadas');
});
module.exports={
    Usuarios,
    Productos,
    Estados,
    Formas_Pagos,
    Detalle_Pedidos,
    Pedidos
}