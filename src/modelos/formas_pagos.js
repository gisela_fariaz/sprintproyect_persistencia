module.exports = (sequelize, type) => {
    return sequelize.define('Formas_pagos', {
        detalle_pago: type.STRING,
    },
        {
            sequelize,
            timestamps: false,//para remover las columnas de createdAt,updatedAt
        }
    )
}
