module.exports = (sequelize, type) => {
    return sequelize.define('Detalle_Pedidos', {
        cantidad: type.FLOAT,
    },
        {
            sequelize,
            timestamps: false,//para remover las columnas de createdAt,updatedAt
        }
    )
}
