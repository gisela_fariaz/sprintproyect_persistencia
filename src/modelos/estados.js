module.exports = (sequelize, type) => {
    return sequelize.define('Estados', {
        estado: type.STRING,
    },
        {
            sequelize,
            timestamps: false,//para remover las columnas de createdAt,updatedAt
        }
    )
}
