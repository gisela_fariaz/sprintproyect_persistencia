module.exports = (sequelize, type) => {
    return sequelize.define('Productos', {
        detalle: type.STRING, 
        precio: type.FLOAT,
    },
        {
            sequelize,
            timestamps: false,//para remover las columnas de createdAt,updatedAt
        }
    )
}