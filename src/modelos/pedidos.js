module.exports = (sequelize, type) => {
    return sequelize.define('Pedidos', {
        total: type.FLOAT,
        hora:{
            type: type.DATE,
            defaultValue: type.NOW
        }
    },
        {
            sequelize,
            timestamps: false,//para remover las columnas de createdAt,updatedAt
        }
    )
}
