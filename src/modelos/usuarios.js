module.exports = (sequelize, type) => {
    return sequelize.define('Usuarios', {
        nom_ape: type.STRING,
        correo: type.STRING,
        tel: type.INTEGER,
        direccion: type.STRING,
        pass: type.STRING,
        admin: {
            type:type.BOOLEAN,
            defaultValue:0
        },
        estado:{
            type:type.BOOLEAN,
            defaultValue:0
        }
    },
        {
            sequelize,
            timestamps: false,//para remover las columnas de createdAt,updatedAt
        }
    )
}
